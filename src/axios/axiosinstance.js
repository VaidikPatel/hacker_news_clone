import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: 'http://13.233.168.206:8080'
});

export default axiosInstance;
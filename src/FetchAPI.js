import axios from "./axios/axiosinstance";

export async function fetchNewPosts() {
  const responseFetchNewPosts = await axios.get(`/posts/new`);
  const dataFetchNewPosts = responseFetchNewPosts.data;
  return dataFetchNewPosts;
}
//_.get()
export async function fetchUser(userId) {
  const responseFetchUser = await axios.get(`/user/${userId}`);
  const dataFetchUser = await responseFetchUser.data;
  return dataFetchUser;
}

export async function fetchUserPosts(userId) {
  const responseFetchUserPosts = await axios.get(`/posts/user/${userId}/false`);
  const dataFetchUserPosts = await responseFetchUserPosts.data;
  return dataFetchUserPosts;
}

export async function fetchComments() {
  const responseFetchComments = await axios.get(`/comments/new`);
  const dataFetchComments = await responseFetchComments.data;
  return dataFetchComments;
}

export async function fetchPostById(postId) {
  const responseFetchPostById = await axios.get(`/post/${postId}`);
  const dataFetchPostById = await responseFetchPostById.data;
  return dataFetchPostById;
}

export async function fetchUserComments(postId) {
  const responseFetchUserComments = await axios.get(`/comments/post/${postId}`);
  const dataFetchUserComments = await responseFetchUserComments.data;
  return dataFetchUserComments;
}

export async function authenticate(props) {
  const {username, password, isSignIn} = props;
  const url = isSignIn ? "/login" : "/signup";
  const requestBody = {
    username,
    password,
  };
  const authPromise = await axios.post(url, requestBody);
  const authData = await authPromise.data;
  return authData;
}

export async function AddComment(obj) {
  console.log("In Add Comment post API call !!!");
  console.log("Obj: ",obj);
  const config = {
    headers: {
      Authorization : `Bearer ${obj.token}`,
    }
  }
  const responseAddComment = await axios.post(`/comment`,obj,config);
  const dataAddComment = await responseAddComment.data;
  return dataAddComment;
}

export async function AddPost(obj) {
  console.log("In Add Post post API call !!!");
  console.log("Obj: ",obj);
  const config = {
    headers: {
      Authorization : `Bearer ${obj.token}`,
    }
  }
  const responseAddPost = await axios.post(`/post`,obj,config);
  const dataAddPost = await responseAddPost.data;
  return dataAddPost;
}

export async function UpVotePost(postId,userId,token) {
  const config = {
    headers: {
      Authorization : `Bearer ${token}`,
    }
  }
  const responseUpVotePost = await axios.put(`/post/vote/${postId}/${userId}`,null,config);
  const dataUpVotePost = await responseUpVotePost.data;
  return dataUpVotePost;
}

export async function UpVoteComment(commentId,userId,token) {
  const config = {
    headers: {
      Authorization : `Bearer ${token}`,
    }
  }
  const responseUpVoteComment = await axios.put(`/comment/vote/${commentId}/${userId}`,null,config);
  const dataUpVoteComment = await responseUpVoteComment.data;
  return dataUpVoteComment;
}

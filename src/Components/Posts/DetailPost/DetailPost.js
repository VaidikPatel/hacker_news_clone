import React from "react";
import { useLocation, useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import "./DetailPost.css";
import Comment from "../../Comments/Comment/Comment";
import Post from "../Post/Post";
import {
  fetchPostById,
  fetchUserComments,
  AddComment,
} from "../../../FetchAPI";
import { getRandomId } from "../../../Utils/AllUtils";

function DetailPost(props) {
  const [post, setPost] = React.useState({});
  const [postLoading, setPostLoading] = React.useState(true);
  const [commentValue, setCommentValue] = React.useState("");
  const [userComments, setUserComments] = React.useState([]);
  const [userCommentsLoading, setUserCommentsLoading] = React.useState(true);
  const location = useLocation();
  const history = useHistory();
  const postId = new URLSearchParams(location.search).get("id");
  const userId = useSelector((state) => state.auth.userId);
  const username = useSelector((state) => state.auth.username);
  const token = useSelector((state) => state.auth.token);

  const getPostById = React.useCallback(() => {
    fetchPostById(postId)
      .then((PostData) => {
        setPost(PostData);
        setPostLoading(false);
      })
      .catch((error) => {
        console.log(`Error in fetching Post of postId: ${postId} !`, error);
        setPostLoading(false);
      });
  }, [postId]);

  const getUserComments = React.useCallback(() => {
    fetchUserComments(postId)
      .then((UserCommentsData) => {
        setUserComments(UserCommentsData);
        setUserCommentsLoading(false);
      })
      .catch((error) => {
        console.log(`Error in fetching ${post.author} 's comments !`, error);
        setUserCommentsLoading(false);
      });
  }, [postId, post.author]);

  React.useEffect(() => {
    getPostById();
    getUserComments();
    document.title = "HackerNews | Perticular Post";
  }, [getPostById, getUserComments]);

  const redirectToLogin = React.useCallback(() => {
    console.log("redirect to login !!! in detail Post");
    if (!userId) {
      console.log("forwar to auth Page by detail post");
      const pathname = location.pathname.substr(1);
      history.push({
        pathname: "/auth",
        search: pathname === "" ? "" : `?goto=${pathname}${location.search}`,
      });
      return true;
    }
    return false;
  }, [userId, location.pathname, location.search, history]);

  const onSubmitCommentHandler = React.useCallback(
    (event) => {
      event.preventDefault();

      if (commentValue.length < 6) {
        return;
      }

      if (!redirectToLogin()) {
        console.log("Already loggedin in Add Comment Part");
        AddComment({
          author: username,
          commentBody: commentValue,
          commentId: getRandomId(6),
          createdOn: new Date(),
          points: 0,
          postId: postId,
          upVotedBy: [],
          userId: userId,
          token: token,
        })
          .then((addCommentResponse) => {
            console.log(addCommentResponse);
            setCommentValue("");
            getUserComments();
          })
          .catch((error) => {
            console.log(error);
          });
      }
    },
    [
      redirectToLogin,
      getUserComments,
      commentValue,
      postId,
      userId,
      username,
      token,
    ]
  );

  const onCommentInputChangeHandler = (event) => {
    event.preventDefault();
    setCommentValue(event.target.value);
  };

  return (
    <div>
      {!postLoading ? (
        <table>
          <Post key={post.postId} {...post} />
          <tbody>
            <tr>
              <td colSpan="2"></td>
              <td>
                <form onSubmit={onSubmitCommentHandler}>
                  <textarea
                    cols="55"
                    rows="5"
                    onChange={onCommentInputChangeHandler}
                    value={commentValue}
                  ></textarea>
                  <div id="add-comment-div">
                    <button type="submit" disabled={commentValue.length < 6}>
                      Add Comment
                    </button>
                  </div>
                </form>
              </td>
            </tr>
          </tbody>
        </table>
      ) : (
        "Loading..."
      )}
      <br/>
      <br/>
      {userCommentsLoading ? (
        "Loading..."
      ) : (
        <table>
          {userComments.map((comment) => (
            <Comment key={comment.commentId} {...comment} />
          ))}
        </table>
      )}
    </div>
  );
}

export default React.memo(DetailPost);

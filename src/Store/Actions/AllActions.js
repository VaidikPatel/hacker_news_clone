import { AUTHENTICATION_SUCCESS, LOGOUT } from "./AllActionTypes";

export const authSuccess = (props) => {
  console.log("In authsuccess action !!! ",props);
  const {token, userId, username, userRole} = props;
        console.log("username: ",username);
        console.log("userId: ",userId);
        console.log("userRole: ",userRole);
        console.log("token: ",token);
  localStorage.setItem("token",token);
  localStorage.setItem("userId",userId);
  localStorage.setItem("username",username);
  localStorage.setItem("userRole",userRole);
  return {
    type: AUTHENTICATION_SUCCESS,
    payload: { token, userId, username, userRole },
  };
};

export const logout = () => {
  console.log("In logout action !!!!");
  localStorage.removeItem("token");
  localStorage.removeItem("userId");
  localStorage.removeItem("username");
  localStorage.removeItem("userRole");
  return {
    type: LOGOUT,
  };
};

import React, { useCallback } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { useDispatch } from "react-redux";
import "./App.css";
import Nav from "./Components/Navigation/Nav";
import NewPosts from "./Components/Posts/NewPosts";
import News from "./News";
import Comments from "./Components/Comments/Comments";
import Submit from "./Components/Submit/Submit";
import Auth from "./Authentication/Auth";
import User from "./Components/User/user";
import DetailPost from "./Components/Posts/DetailPost/DetailPost";
import Logout from "./Authentication/Logout";
import { authSuccess } from "./Store/Actions/AllActions";

function App() {
  const dispatch = useDispatch();

  const checkForLoggedIn = useCallback(() => {
    const token = localStorage.getItem("token");
    console.log("In App: ", token);
    if (token) {
      const username = localStorage.getItem("username");
      const userId = localStorage.getItem("userId");
      const userRole = localStorage.getItem("userRole");
      console.log("go for authsuccess !!!");
      dispatch(authSuccess({ token, username, userRole, userId }));
    }
  }, [dispatch]);

  React.useEffect(() => {
    checkForLoggedIn();
  }, [checkForLoggedIn]);

  return (
    <Router>
      <div className="App">
        <Nav />
        <Switch>
          <Route path="/" exact component={NewPosts} />
          <Route path="/news" exact component={News} />
          <Route path="/newest" component={NewPosts} />
          <Route path="/comments" component={Comments} />
          <Route path="/submit" component={Submit} />
          <Route path="/auth" component={Auth} />
          <Route path="/signout" component={Logout} />
          <Route path="/user" component={User} />
          <Route path="/post" component={DetailPost} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;

import React from "react";
import "./Comments.css";
import Comment from "./Comment/Comment";
import { fetchComments } from "../../FetchAPI";

function Comments() {
  const [comments, setComments] = React.useState([]);

  const getComments = React.useCallback(() => {
    fetchComments()
      .then((CommentsData) => {
        setComments(CommentsData);
      })
      .catch((error) => {
        console.log("Error in fetching Comments: ", error);
      });
  }, []);

  React.useEffect(() => {
    getComments();
    document.title = "HackerNews | Comments";
  }, [getComments]);

  return (
    <div className="Comments">
      <table>
        {comments.size === 0 ? (
          <tbody>
            <tr>
              <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
              <td>No Comments found</td>
            </tr>
          </tbody>
        ) : (
          comments.map((comment) => (
            <Comment key={comment.commentId} {...comment} />
          ))
        )}
      </table>
    </div>
  );
}

export default React.memo(Comments);

import { AUTHENTICATION_SUCCESS, LOGOUT } from "../Actions/AllActionTypes";

const initialState = {
  token: null,
  userId: null,
  username: null,
  userRole: null,
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTHENTICATION_SUCCESS:
      console.log("In Auth Success Reducer !!!!");
      console.log("token",action.payload.token);
      console.log("userId",action.payload.userId);
      console.log("username",action.payload.username);
      console.log("userRole",action.payload.userRole);
      return {
        token: action.payload.token,
        userId: action.payload.userId,
        username: action.payload.username,
        userRole: action.payload.userRole,
      };
    case LOGOUT:
      console.log("In logout in auth reducer !!!");
      return {
        token: null,
        userId: null,
        username: null,
        userRole: null,
      };
    default:
      return state;
  }
};

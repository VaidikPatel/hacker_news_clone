import React from "react";
import { Redirect } from "react-router-dom";
import { useDispatch } from "react-redux";
import { logout } from "../Store/Actions/AllActions";

export default function Logout(props) {
  const params = new URLSearchParams(props.location.search);
  const redirectURL = params.get("goto");
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(logout());
  });

  return <Redirect to={redirectURL ? "/" + redirectURL : ""} />;
}

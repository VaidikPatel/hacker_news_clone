export const getDifferenceBetweenDates = (startDate, endDate) => {
    const diff = endDate.getTime() - startDate.getTime();
    const days = diff/(24*60*60*1000)
    if(days > 1){
        return days.toFixed(0) + " days ago"
    }
    const hours = diff/(60*60*1000)
    if(hours > 1){
        return hours.toFixed(0) + " hours ago"
    }
    return (diff / 60000).toFixed(0) + " minutes ago";
}

export const formatDate = (date) => {
    const options = {year: 'numeric', month: 'long', day: 'numeric'};
    return date.toLocaleDateString("en-US", options)
}
import { authReducer } from "./Auth";
import { combineReducers } from "redux";


const allReducers = combineReducers({
    auth : authReducer,
});

export default allReducers;
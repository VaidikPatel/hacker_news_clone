import React from "react";
import "../../App.css";
import Post from "./Post/Post";
import { fetchNewPosts } from "../../FetchAPI";
import { addItemsToListOfPosts } from "../../Utils/AllUtils";

function NewPosts() {
  const [newPosts, setNewPosts] = React.useState([]);

  const getNews = React.useCallback(() => {
    fetchNewPosts()
      .then((newPostsData) => {
        setNewPosts(addItemsToListOfPosts(newPostsData));
      })
      .catch((error) => {
        console.log("Error in fetching news: ", error);
      });
  }, []);

  React.useEffect(() => {
    getNews();
    document.title = "HackerNews | New Posts";
  }, [getNews]);

  return (
    <div className="Posts">
      <table>
        {newPosts.size === 0 ? (
          <tbody>
            <tr>
              <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
              <td>No Posts found</td>
            </tr>
          </tbody>
        ) : (
          newPosts.map((newPost) => <Post key={newPost.postId} {...newPost} showId={true} />)
        )}
      </table>
    </div>
  );
}

export default React.memo(NewPosts);

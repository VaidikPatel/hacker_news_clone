import React from "react";
import { Redirect, useHistory, useLocation } from "react-router";
import { useSelector } from "react-redux";
import { getRandomId } from "../../Utils/AllUtils";
import { AddPost } from "../../FetchAPI";
function Submit(props) {
  console.log("In Submit Page: ", props);
  const userId = useSelector((state) => state.auth.userId);
  const username = useSelector((state) => state.auth.username);
  const token = useSelector((state) => state.auth.token);
  const [title, setTitle] = React.useState("");
  const [url, setUrl] = React.useState("");
  const [isValidUrl, setIsValidUrl] = React.useState(false);
  const [isValidForm, setIsValidForm] = React.useState(false);
  const location = useLocation();
  const history = useHistory();
  const pathname = location.pathname.substr(1);
  const search = location.search;
  console.log("location: ", location);
  console.log("history: ", history);
  const regex = new RegExp(
    /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi
  );
  const redirectToAuthPage = React.useMemo(() => {
    const obj = {
      pathname: "/Auth",
      search: pathname !== "" ? `?goto=${pathname}${search}` : ``,
    };
    return obj;
  }, [pathname, search]);
  const isLoggedIn = useSelector((state) => state.auth.username !== null);

  const onTitleChangeHandler = (event) => {
    event.preventDefault();
    const titleInput = event.target.value;
    setTitle(titleInput);
    title.length >= 5 && isValidUrl
      ? setIsValidForm(true)
      : setIsValidForm(false);
  };

  const onUrlChangeHandler = (event) => {
    event.preventDefault();
    const UrlInput = event.target.value;
    setUrl(UrlInput);
    const isValid = regex.test(UrlInput);
    setIsValidUrl(isValid);

    title.length >= 5 && isValidUrl
      ? setIsValidForm(true)
      : setIsValidForm(false);
  };

  const redirectToLogin = React.useCallback(() => {
    if (!userId) {
      history.push(redirectToAuthPage);
      return true;
    }
    return false;
  }, [userId, history, redirectToAuthPage]);

  const onSubmitHandler = React.useCallback(
    (event) => {
      event.preventDefault();
      if (!redirectToLogin()) {
        AddPost({
          author: username,
          comments: 0,
          createdOn: new Date(),
          flagged: false,
          points: 0,
          postId: getRandomId(6),
          reported: false,
          reportedBy: [],
          title: title,
          upVotedBy: [],
          url: url,
          userId: userId,
          token: token,
        })
          .then((responseData) => {
            console.log(responseData);
            history.push({
              pathname: "/newest",
            });
          })
          .catch((error) => {
            console.log(error);
          });
      }
    },
    [username, userId, token, title, url, redirectToLogin, history]
  );
  return (
    <>
      <h1>Submit Page !!!</h1>
      {!isLoggedIn && <Redirect to={redirectToAuthPage} />}
      <form onSubmit={onSubmitHandler}>
        <table>
          <tbody>
            <tr>
              <td>title</td>
              <td>
                <input
                  type="text"
                  placeholder="Enter title ... [length >=5]"
                  onChange={onTitleChangeHandler}
                  value={title}
                />
              </td>
            </tr>
            <tr>
              <td>url</td>
              <td>
                <input
                  type="text"
                  placeholder="Enter valid URL ... "
                  onChange={onUrlChangeHandler}
                  value={url}
                />
              </td>
            </tr>
            <tr>
              <td colSpan="1" />
              <td>
                <button type="submit" disabled={!isValidForm}>
                  Submit
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    </>
  );
}

export default React.memo(Submit);

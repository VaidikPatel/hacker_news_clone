import React from "react";
import { Link, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import "./Nav.css";
import Pipe from "../Pipe";
import logo from "../../Assets/Images/y18.gif";

function Nav() {
  const username = useSelector(state => state.auth.username);
  const userId = useSelector(state => state.auth.userId);
  const location = useLocation();
  const pathName = location.pathname.substr(1);
  return (
    <nav>
      <div className="nav-links">
        <div className="links-left">
          <Link to="/news">
            <img src={logo} alt="Logo of HackerNews" id="logo" />
          </Link>
          <Link to="/news">
            <li>
              <strong>HackerNews</strong>
            </li>
          </Link>
          <Pipe />
          <Link to="/newest">
            <li>new</li>
          </Link>
          <Pipe />
          <Link to="/oldest">
            <li>past</li>
          </Link>
          <Pipe />
          <Link to="/comments">
            <li>comments</li>
          </Link>
          <Pipe />
          <Link to="/ask">
            <li>ask</li>
          </Link>
          <Pipe />
          <Link to="/shop">
            <li>shop</li>
          </Link>
          <Pipe />
          <Link to="/jobs">
            <li>jobs</li>
          </Link>
          <Pipe />
          <Link to="/submit">
            <li>submit</li>
          </Link>
        </div>
        <div className="links-right">
          {username === null ? (
            <Link
              className="nav-links"
              to={{
                pathname: "/auth",
                search:
                  pathName === "auth" || pathName === ""
                    ? ""
                    : "?goto=" + pathName,
              }}
            >
              <span>logins</span>
            </Link>
          ) : (
            <>
              <Link
                className="nav-links"
                to={{
                  pathname: "/user",
                  search: "?userId=" + userId,
                }}
              >
                <span>{username}</span>
              </Link>
              <span className="navbar-item-separator">|</span>
              <Link
                className="nav-links"
                to={{
                  pathname: "/signout",
                  search: "?goto=" + pathName + location.search,
                }}
              >
                <span>logout</span>
              </Link>
            </>
          )}
        </div>
      </div>
    </nav>
  );
}

export default React.memo(Nav);

import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Redirect } from "react-router-dom";
import "./Auth.css";
import { authenticate } from "../FetchAPI";
import { authSuccess } from "../Store/Actions/AllActions";

function Auth(props) {
  console.log("In Auth Page!!!");
  const [isValid, setIsValid] = React.useState(false);
  const [isSignIn, setIsSignIn] = React.useState(true);
  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [error, setError] = React.useState(false);
  const [errorMessage, setErrorMessage] = React.useState("");

  const isLoggedIn = useSelector((state) => state.auth.username !== null);

  const params = new URLSearchParams(props.location.search);
  const redirectURL = params.get("goto");
  console.log("isloggedin: ", isLoggedIn);
  const dispatch = useDispatch();
  //React.useEffect(() => {}, []);
  const changeAuthType = (event) => {
    event.preventDefault();
    setIsSignIn((isSignIn) => !isSignIn);
    setError(false);
  };

  function onSubmitHandler(event) {
    event.preventDefault();
    console.log("before auth api call !!!");
    console.log("username: ",username);
    console.log("password: ",password);
    console.log("isSignIn: ",isSignIn);
    authenticate({
      username,
      password,
      isSignIn,
    })
      .then((response) => {
        const token = response.token;
        const username = response.username;
        const userId = response.userId;
        const userRole = response.userRole;
        console.log("get response from auth api !!!");
        console.log("username: ",username);
        console.log("userId: ",userId);
        console.log("userRole: ",userRole);
        console.log("token: ",token);
        dispatch(authSuccess({token, userId, username, userRole}));
      })
      .catch((error) => {
        setError(true);
        console.log(error.response);
        setErrorMessage(error?.response?.data?.message);
      });
  }

  function usernameChangeHandler(event) {
    const usernameInput = event.target.value;
    setUsername(usernameInput);
    if (usernameInput.length > 5 && password.length >= 6) {
      setIsValid(true);
    } else {
      setIsValid(false);
    }
  }

  function passwordChangeHandler(event) {
    const passwordInput = event.target.value;
    setPassword(passwordInput);
    if (passwordInput.length >= 6 && username.length > 5) {
      setIsValid(true);
    } else {
      setIsValid(false);
    }
  }

  return (
    <div className="master-div">
      {isLoggedIn ? (
        <Redirect to={redirectURL ? "/" + redirectURL : ""} />
      ) : null}
      <h1>Authentication</h1>
      <h2>You have to be logged in order to submit.</h2>
      <div id="error">
        {" "}
        {error
          ? isSignIn
            ? "Incorrect username or password"
            : errorMessage
          : null}
      </div>
      <form onSubmit={onSubmitHandler}>
        <div className="label">username</div>
        <input
          type="text"
          onChange={usernameChangeHandler}
          value={username}
          className="input"
        ></input>
        <div className="label">password</div>
        <input
          type="password"
          onChange={passwordChangeHandler}
          value={password}
          className="input"
        ></input>
        <div>
          <button
            type="submit"
            disabled={!isValid}
            className={"button button1 " + (isValid ? "" : "disableButton")}
          >
            {isSignIn ? "SIGN IN" : "SIGN UP"}
          </button>
          <button onClick={changeAuthType} className="button button2">
            {isSignIn ? "SWITCH TO SIGN UP" : "SWITCH TO SIGN IN"}
          </button>
        </div>
      </form>
    </div>
  );
}

export default Auth;

export const addItemsToListOfPosts = (response) => {
  const newResponse = response.map((item, index) => {
    const urlWithHttp =
      item.url.indexOf("https://") === -1 && item.url.indexOf("http://") === -1
        ? "http://" + item.url
        : item.url; 
    return { ...item, id: index + 1, domain: new URL(urlWithHttp).hostname };
  });
  console.log(newResponse);
  return newResponse;
};

export const getRandomId = (length) => {
    return Math.random().toString(32).substr(2, length);
}
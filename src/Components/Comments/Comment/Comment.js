import React from "react";
import { Link, useHistory, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import "./Comment.css";
import { getDifferenceBetweenDates } from "../../../Utils/DateUtils";
import arrow from "../../../Assets/Images/grayarrow.gif";
import { UpVoteComment } from "../../../FetchAPI";

function Comment(props) {
  const location = useLocation();
  const history = useHistory();
  const userId = useSelector((state) => state.auth.userId);
  const token = useSelector((state) => state.auth.token);
  const upVote = userId && props.upVotedBy.includes(userId) ? false : true;
  const [showUpVote, setShowUpVote] = React.useState(upVote);
  const [point, setpoint] = React.useState(props.points);
  const divider = <>&nbsp;|&nbsp;</>;

  const redirectToLogin = React.useCallback(() => {
    console.log("userId: ",userId);
    if (!userId) {
      console.log("required to redirect to login page !!!!");
      const pathname = location.pathname.substr(1);
      console.log("pathname: ",pathname,"locationsearch: ",location.search);
      history.push({
        pathname: "/auth",
        search: pathname === "" ? "" : `?goto=${pathname}${location.search}`,
      });
      return true;
    }
    return false;
  }, [userId, location.pathname, location.search, history]);

  const onClickUpVote = (event) => {
    event.preventDefault();
    console.log("Click on UpVote !!!");
    if (!redirectToLogin()) {
      UpVoteComment(props.commentId, userId, token)
        .then((response) => {
          setpoint((prev) => prev + 1);
        })
        .catch((err) => {
          console.log(err);
        });
      setShowUpVote((previousValue) => !previousValue);
    }
  };
  const onClickDownVote = (event) => {
    event.preventDefault();
    console.log("Click on UpVote !!!");
    if (!redirectToLogin()) {
      UpVoteComment(props.commentId, userId, token)
        .then((response) => {
          setpoint((prev) => prev - 1);
        })
        .catch((err) => {
          console.log(err);
        });
      setShowUpVote((previousValue) => !previousValue);
    }
  };
  const upVoteButton =
    userId && props.userId === userId ? (
      "+"
    ) : userId && props.upVotedBy.includes(userId) ? null : (
      <div
        title="upvote"
        onClick={onClickUpVote}
        style={{ visibility: showUpVote ? "visible" : "hidden" }}
      >
        <img src={arrow} alt="arrow" />
      </div>
    );
  return (
    <tbody className="Comment">
      <tr>
        <td>{upVoteButton}</td>
        <td>
          <Link
            to={{
              pathname: "/user",
              search: `?userId=${props.userId}`,
            }}
          >
            {props.author}
          </Link>
          {divider}
          {point} points
          {divider}
          {getDifferenceBetweenDates(new Date(props.createdOn), new Date())}
          {divider}
          <Link
            to={{
              pathname: "/post",
              search: `?id=${props.postId}`,
            }}
          >
            Parent
          </Link>
          {divider}
          {!showUpVote ? (
            <>
              <a href={window.location} onClick={onClickDownVote}>
                unvote
              </a>
            </>
          ) : null}
        </td>
      </tr>
      <tr>
        <td colSpan="1" />
        <td>{props.commentBody}</td>
      </tr>
      <tr style={{ height: "30px" }} />
    </tbody>
  );
}

export default React.memo(Comment);

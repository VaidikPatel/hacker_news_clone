import React from "react";
import { useLocation } from "react-router-dom";
import { fetchUser, fetchUserPosts } from "../../FetchAPI";
import { formatDate } from "../../Utils/DateUtils";
import { addItemsToListOfPosts } from "../../Utils/AllUtils";
import "./user.css";
import Post from "../Posts/Post/Post";

function User(props) {
  const [user, setUser] = React.useState({});
  const [isUserLoading, setIsUserLoading] = React.useState(true);
  const [isShowUserPosts, setIsShowUserPosts] = React.useState(false);
  const [userPosts, setUserPosts] = React.useState([]);
  const [isUserPostsLoading, setIsUserPostsLoading] = React.useState(true);

  const location = useLocation();
  const userId = new URLSearchParams(location.search).get("userId");

  React.useEffect(() => {
    fetchUser(userId)
      .then((userResponse) => {
        console.log(userResponse);
        setUser(userResponse);
        setIsUserLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setIsUserLoading(false);
      });
  }, [userId]);

  const showUserPosts = () => {
    fetchUserPosts(userId)
      .then((userPostsResponse) => {
        console.log(userPostsResponse);
        userPostsResponse = addItemsToListOfPosts(userPostsResponse);
        setUserPosts(userPostsResponse);
        setIsUserPostsLoading(false);
        setIsShowUserPosts(true);
      })
      .catch((error) => {
        console.log(error);
        setIsUserPostsLoading(false);
      });
  };

  const hideUserPosts = () => {
    setIsShowUserPosts(false);
  };

  return (
    <>
      <table id="user-table">
        <tbody key="details" className="user-details">
          {isUserLoading ? (
            <tr>
              <td>...Loading</td>
            </tr>
          ) : (
            <>
              <tr>
                <td>username:</td>
                <td>{user.username}</td>
              </tr>
              <tr>
                <td>created On :</td>
                <td>{formatDate(new Date(user.createdOn))}</td>
              </tr>
            </>
          )}
        </tbody>
        <tbody key="posts" className="user-posts">
          <tr>
            <td>
              {!isShowUserPosts ? (
                <span
                  id="show-link"
                  onClick={showUserPosts}
                >{`See ${user.username} 's All Posts`}</span>
              ) : (
                <span
                  id="show-link"
                  onClick={hideUserPosts}
                >{`Hide ${user.username} 's All Posts`}</span>
              )}
            </td>
          </tr>
        </tbody>
      </table>
      <table id="user-posts">
          {!isShowUserPosts ? null : isUserPostsLoading ? (
            <h1>Loading User's Posts !!!</h1>
          ) : (
            userPosts.map((newPost) => (
              <Post key={newPost.postId} {...newPost} />
            ))
          )}
      </table>
    </>
  );
}

export default React.memo(User);

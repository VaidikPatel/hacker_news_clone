import React from 'react';
import '../App.css';

function Pipe() {
    return (
        <li className='pipe-separator'>|</li>
    );
}

export default React.memo(Pipe);
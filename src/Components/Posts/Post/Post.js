import React from "react";
import { Link, useHistory, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import "./Post.css";
import arrow from "../../../Assets/Images/grayarrow.gif";
import { getDifferenceBetweenDates } from "../../../Utils/DateUtils";
import { UpVotePost } from "../../../FetchAPI";

function Post(props) {
  console.log("In Post: ", props);

  const [point, setpoint] = React.useState(props.points);
  const location = useLocation();
  const history = useHistory();
  const userId = useSelector((state) => state.auth.userId);
  const upVote = userId && props.upVotedBy.includes(userId) ? false : true;
  const [showUpVote, setShowUpVote] = React.useState(upVote);
  const token = useSelector((state) => state.auth.token);

  const redirectToLogin = () => {
    if (!userId) {
      console.log("required to redirect to login page !!!!");
      const pathname = location.pathname.substr(1);
      history.push({
        pathname: "/auth",
        search: pathname === "" ? "" : `?goto=${pathname}${location.search}`,
      });
      return true;
    }
    return false;
  };

  const onClickUpVote = (event) => {
      event.preventDefault();
      console.log("Click on UpVote !!!");
      if (!redirectToLogin()) {
        UpVotePost(props.postId, userId, token)
          .then((response) => {
            setpoint((prev) => prev + 1);
          })
          .catch((err) => {
            console.log(err);
          });
        setShowUpVote((previousValue) => !previousValue);
      }
    };

  const onClickDownVote = (event) => {
      event.preventDefault();
      console.log("Click on DownVote !!!");
      if (!redirectToLogin()) {
        UpVotePost(props.postId, userId, token)
          .then((response) => {
            setpoint((prev) => prev - 1);
          })
          .catch((err) => {
            console.log(err);
          });
        setShowUpVote((previousValue) => !previousValue);
      }
    };
  const upVoteButton = (
    userId && props.userId === userId ? 
      "+"
     : userId && props.upVotedBy.includes(userId) ? null : 
      <span
        title="upvote"
        onClick={onClickUpVote}
        style={{
          visibility: !userId ? "visible" : showUpVote ? "visible" : "hidden",
        }}
      >
        <img src={arrow} alt="arrow" />
      </span>
  );

  const divider = <>&nbsp;|&nbsp;</>;
  return (
    <>
      <tbody className="Post">
        <tr>
          <td>
            {(props.showId) ? <span className="rank">{`${props.id}.`}</span> : null}
          </td>
          <td>{upVoteButton}</td>
          <td>
            <span className="title">
              <a href={props.url} target="_blank" rel="noreferrer">
                {props.title}
              </a>
            </span>
            &nbsp;
            <span className="domain">
              <a
                href={"https://" + props.domain}
                target="_blank"
                rel="noreferrer"
              >
                {props.domain}
              </a>
            </span>
          </td>
        </tr>
        <tr>
          <td colSpan="2" />
          <td>
            <span className="sub_text">
              {point} points by&nbsp;
              <Link
                to={{
                  pathname: "/user",
                  search: `?userId=${props.userId}`,
                }}
              >
                {props.author}
              </Link>
              &nbsp;
              {getDifferenceBetweenDates(new Date(props.createdOn), new Date())}
              {divider}
              {!showUpVote ? (
                <>
                  <a href={window.location} onClick={onClickDownVote}>
                    unvote
                  </a>
                  {divider}
                </>
              ) : null}
              <Link
                id="news-username-link"
                to={{
                  pathname: "/post",
                  search: "?id=" + props.postId,
                }}
              >
                {props.comments} comments
              </Link>
            </span>
          </td>
        </tr>
        <tr style={{ height: "5px" }} />
      </tbody>
    </>
  );
}

export default React.memo(Post);
